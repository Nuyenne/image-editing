package pdl.backend;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;	//own addition
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;	//own addition

//import org.apache.tomcat.util.http.parser.MediaType;
import org.springframework.http.MediaType;	//own addition
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.*;	//own addition
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.test.util.ReflectionTestUtils;	//own addition


@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
public class ImageControllerTests {

	@Autowired
	private MockMvc mockMvc;

	@BeforeAll
	public static void reset() {
  	// reset Image class static counter
  	ReflectionTestUtils.setField(Image.class, "count", Long.valueOf(0));
	}

	@Test
	@Order(1)
	public void getImageListShouldReturnSuccess() throws Exception {
		// TODO
		this.mockMvc.perform(get("/images")).andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8));	// deprecated
	}

	@Test
	@Order(2)
	public void getImageShouldReturnNotFound() throws Exception {
		// TODO
		this.mockMvc.perform(get("/images/1")).andDo(print())
			.andExpect(status().isNotFound());
	}

	@Test
	@Order(3)
	public void getImageShouldReturnSuccess() throws Exception {
		// TODO
		this.mockMvc.perform(get("/images/0")).andDo(print())
			.andExpect(status().isOk())
			.andExpect(content().contentType(MediaType.IMAGE_JPEG));
	}

	@Test
	@Order(4)
	public void deleteImagesShouldReturnMethodNotAllowed() throws Exception {
		// TODO

		this.mockMvc.perform(delete("/images/")).andDo(print())
			.andExpect(status().isMethodNotAllowed());
		// undecided whether the whole URL is needed or not (probably not)
	}

	@Test
	@Order(5)
	public void deleteImageShouldReturnNotFound() throws Exception {
		// TODO
		this.mockMvc.perform(delete("/images/1")).andDo(print())
			.andExpect(status().isNotFound());
	}

	@Test
	@Order(6)
	public void deleteImageShouldReturnSuccess() throws Exception {
		// TODO
		this.mockMvc.perform(delete("/images/0")).andDo(print())
			.andExpect(status().isOk());
	}

	@Test
	@Order(7)
	public void createImageShouldReturnSuccess() throws Exception {
		// TODO

		MockMultipartFile test_success = new MockMultipartFile(
			"test_success",
			"test_img.jpg",
			MediaType.IMAGE_JPEG_VALUE,
			"".getBytes());

		this.mockMvc.perform(multipart("/images").file("file", test_success.getBytes()))
			.andExpect(status().isCreated());
	}

	@Test
	@Order(8)
	public void createImageShouldReturnUnsupportedMediaType() throws Exception {
		// TODO

		MockMultipartFile test_file = new MockMultipartFile(
			"test_file",
			"test.txt",
			MediaType.TEXT_PLAIN_VALUE,
			"1, 2. 1, 2. This is a test".getBytes());

		this.mockMvc.perform(multipart("/images").file("file", test_file.getBytes()))
			.andExpect(status().isUnsupportedMediaType());	//200 response instead of 415
	}

}
